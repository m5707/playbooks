# Phishing Playbook

[[_TOC_]]

## Scope
This Playbook covers preparation and detection of phishing attack. 

## 1. Preparation




- Security awareness training within the organization can prevent emploeeys phishing attacks. 

- Create a list of all domains owned by the company
    - Prevent the first responders to take actions against the company owned domain 
    - Employeeys who can registrer domain
        - Contact if unsure of a newly created domain. 


- Create email template for phishinh attack 
    - to notify all employees of ongoing phishing campaing against the organization 
    - Prevent emploeeys to fall for the phishing scheme 
   


### Email template Example: 

    Dear [XXX], 

    This email is to inform you of a secutity incident, which was recently reported relating to phishing attack in [APPLICATION] on [DATE]. 

    We [ORGANIZATION] are aware of the incident, and have launched an investigation. Based on the evidence, we [ACTIONS] to resolve the issue. 

    **What can you do**

    To avoid phishing schmes, please pay attention to the following: 
    - Do not open attechment from unknown senders, be aware of compressed files and executables. 
    - Do not send sensitive information like usernames and passwords over email. 
    - Always inspect URLs before clicking on them. 
    - Do not open shared documents that youre not expecting. 

    If you have any questions regarding the incident, please send an email to [INSERT EMAIL ADDRESS]. 

    Best Regards,   
    [NAME]  
    [ROLE]  
    [COMPANY]  



#

### Assets List
Compile a list of all assets within the organization is crucial
to identify the assets and analyse the likelihood and impact of the assets if exploited. 

  - Risk assessment involves identifying the threats, risks, likelihood and exposure of the
organizational assets. Due, to lack of skilled personnel and technical competency within
small and medium sized enterprises, these following questions are included as a guideline
questions the organization should answer during the risk assessment: 

    - 1. Valuable data within the organization
    - 2. Who handles and where is sensitive data stored 
    - 3. What type of security controls exists within the organization
    - 4. Can any of the identified information lead to a breach within the organization?




  - The organization should at a minimum consider these aspects:
    - Servers
    - Networks
    - Applications
    - Critical Endpoints
    - Employees
    - Security Products
    - Install & Maintain tools 
    - Training

 

</details>

## 2. Detect



-  Objective   
     - Detect incident 
     - Determine scope 
        - Involve third party to analyse, contain, remidate and recover 



 

### Workflow



![Phishing Workflow](Workflow/Phishing.png.png)







### Alerts
- EDR / AV 
    - Endpoint detection and response monitor the end users device accross the network. 
        - Stolen valid credentials normally wont trigger an alarm, however, priviledge elevation and horizonally moving to other systems are likely to get flagged my endpoint detection and response. 
    
- Email Gateway Alert 
- Windows Event Alert 
- File Integrity Alert 



### Notifications
- Employee discovers phishing email 
- Costumer discovers phishing email 
- Internet Service Provider 
- Mail Provider


### Parse and evaluate email 
Email headers containt a lot of information beyond who the email orginated from, which easily can be forged or spoofed by attackers. Email headers contain useful information. 
    
- Outlook     
    -  Double click on email to open in seperate window   
![Outlook](/IRP-PHISHING/Pictures/outlookemailheader.png)
- Gmail   
![Gmail](/IRP-PHISHING/Pictures/gmailphishing.png)


- Parse the headers   
Several onlone tools will convert the email headers and make them easier to parse. 
    - mxtoolbox.com is an example of a website that will assist in parsing headers. 
    - Important fields to check 
        - From addresses 
        - Email route 
        - Mail authentication 
            - SPF
            - DKIM
            - DMARC
        - Spam Score 
        - X-mailer


### Email contains URL or attachments?   
Emails that contains file extenstions such as .exe files for windows, should in general not be opened. However, malware and viruses hide in various types of file extentions such as .msi, .src, .pif, .cmd, .bat, iso. 
Malicious content can however, easily be downloaded to the computer or server by clickling on link/url. URLs should always be validated before clicking on them.   
NB! Links are not always from where they the text indicated, hover over the url/link will indicate which site the link redirects to. 
  
Password protected archives often prevents antivirus software to examine the content. Encrypted arhives such as .zip and .rar should not be extracted unless it validated from trused source.   

Microsoft office documents is a common way for attackers to deliver malicious macros. Users should never enable content, as this is a sophitacted method for attackers to install malicous content on the system.  

**NB!** If Employees have clicked links or opened attachments the incident response team should be contacted while the first resonder further investagate indicators of compromse. 


### Indicators of Compromise 
Indicators of compromise such as attachments and URLs discussed in the section above should be validated. 
- URLs can be checked on various sites such as: 
    - VirusTotal 
    - URLscan 
    - Hybrid Analysis 

- Hashes from files should be validated. 
    - Powershell can be used to get the file hash checksum of files. 
        - get-filehash -path "C:\Users\username\Desktop\fileyouwantocheck" | format-list
        - Output 
            - Algorithm : SHA256
            - Hash      : AB37597D5D19C4C719763BE980F15418EBC9CE862DDBD4EE40040543A2D9691A - This is what you want to check 
            - Path      : C:\Users\username\Desktop\fileyouwantocheck 

    - The hfile hash checksum can be checked towards sites such as: 
        - VirusTotal 
        - Hybrid Analysis 

    - **NB: Adversaries may use sandbox evasion techniques to prevent the malware from running inside a sandbox, meaning that VirusTotal, antivirus solutions, etc. will not detect the file as malicious from its dynamic analysis. For example, the malware might require user interaction to be executed.**

    - Contact the incident response team if any employees have clicked on links or downloaded attachments. In discussion with the incident response team and if the organization has employees with the right skill-set. You can do the following to following to look for malware: 
        - Disk forensics on the comprimised host(s)
        - Log analysis 
        - Network traffic analysis 
        - Malware analysis 
        - Memory analysis 

### Determine Scope of the Incident 
The impact the attack has on the orgnaization should be determined:   
- The impact of the message
- Financial loss 
- Data loss   

The scope of the attack should be investigated, with phishing it is important to consider the number of people: 
- That recived the phishing email 
- Clicked on links 
- Opened attachments 
- Submitted information, usernames, passwords, etc. 
    

### Reporting an Incident, weakness or improvement 
This is an example of how organizations can report and register security inicdents or weakness and ideas for security improvement: 
1. Use corporate email to rport an Information Security Incident 
2. Address the email to "Securityincident@Organization.com" - remember to CC the organizations incident manger. 
3. Subject line: Country, Location, Office, "Event subject" - Incident, weakness, improvement. 
Example: 
    - Norway, Kristiansand: Broken Door - Security weakness 
    - Norway, Bergen: Insider - Data Breach - Security Incident 
4. In the main body of the email, describe the matter of the incident. Including the following: 
    - Time and Date of the security incident 
    - WHO witnessed the security incident 

The email should include as much detail as possible, including what data or systems is involved, the nature of the incident and who to contact for more information. 


 














  



 







</details>
</details>
</details>



</details>

# References
https://www.becybersafe.com/online/email-phishing-advanced.html
https://www.arcsystems.co.uk/the-dangers-of-links-and-attachments-in-emails/
https://blog.focal-point.com/cyber-security-awareness-free-email-templates
https://ransomware.org/how-to-prevent-ransomware/passive-defense/ransomware-backup-strategy/
https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-61r2.pdf
https://www.gov.scot/publications/cyber-resilience-incident-management/
https://cyber.gc.ca/en/guidance/ransomware-playbook-itsm00099
https://gitlab.com/syntax-ir/playbooks
https://www.gov.scot/publications/cyber-resilience-incident-management/  
https://gitlab.com/syntax-ir/playbooks  
https://www.gov.scot/publications/cyber-resilience-incident-management/  


