# Table of Content
[[_TOC_]]

# IR Playbooks

The folders are strcutured and boken down into sections, which are based on NIST 800.61 r2(https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-61r2.pdf). The design of the playbooks are based on Mathieu Saulnier idea. 
## 1 - Planning

### Policy 
Incident response policies are often individualized within organization. However, organ-
isation tend to include the same key elements. The incident response policy should out-
line the organisations response to a security incident. The policy should contain inform-
ation about the structure of the incident response team, the roles of the team members,
employees responsible for testing the efficiency of the policy and, the technological re-
sources, tools that will be used to recover and identify compromised data. According to NIST Special Publication 800-61-R2 the key elements below should be included in the incident response policy:

  - **Statement of Management Commitment & Purpose and Objectives of the policy**
      - Should include high-level statement, including management intent 
      - Requirements and guidance for achievable outcomes
      - Enforced by standards and further implemented by procedures 
      - Objectives should cover what the policy aims to achieve 


  - **Roles and Responsibilities**
    - Enhance the response process by difining roles which responsibility each roles are responsible for. Example of incident response roles:  
      - Incident Commander 
      - Tech Lead 
      - Incident Responders 
      - Communcation Manager 

  - **Prioritization or Severity Ratings of Incidents**
      - Prioritization of incidents on a first come first serve basis can have major impact on buisness operations, incidents should be prioritized the based on severity and impact it has on the organization. Relevant factors to consider: 
        - Functional impact 
          - Incident that directly impact IT-systems and system users. 
        - Informational impact 
          - Incident that affect confidentiality, integrity or avalilability of organizations informational assets 
        - Recoery of the incident  
          - Size and resources needed to recover from the incident 
      - The sevirity ratings should consider all aspects ranging from functionality to breah of confidentility or integrity. The recoverability of the incident should determine how the incident response team handles the incident. I.e, incident that has high impact on the organization and require low efforts to recover from, should trigger an immediate soltion from the IR-team. Example: 



      | **Severity** | **Description**                     | **Example**                                                             |
      |--------------|-------------------------------------|-------------------------------------------------------------------------|
      | 1            | High Impact / Critical incident     | Availability of public-facing service is down for all costumers         |
      | 2            | Significant Impact / Major incident | Availability of public-facing service is down for a subset of costumers |
      | 3            | Low Impact / Minor Incident         | Minor inconvenience for costumers                                       |

    
      1.  The incident have high impact on the organisation and the incident is classified as
          critical. The public facing service is down for all costumers
      2. The incident have a significant impact on the organisation and is classified as a
         major incident. The public facing service is down for a subset of costumers, but not
         all.
      3. The incident have a low impact on the organisation and the incident is classified as
         a minor incident. The public facing service may cause inconvenience for costumers,
         but the service is functional

  - **Definition of Computer Security Incidents and Related Terms**
    - Definitions of unique terms should be clearly defined in the policy and provide the reader
      with a broader understanding of unfamiliar and technical terms
    - Clearly defined terms and definitions reduce the risk of ambiguity and and make inter-
pretations of the policy easier and concise
    - Example: 
      The National Institute of Standards and Technology (NIST, n.d.) defines computer security
incident as: _"Anomalous or unexpected event, set of events, condition, or situation at any
time during the life cycle of a project, product, service, or system"
 
 
  - **Reporting and contact forms**
    - The organizations main communication channels may be breached during an incident,
therefor traditional communication platform such as email or voice of internet protocol
like teams may not be sufficient
    - Contact forms with alternative means of contact should
be created within the organization. The contact forms should include who needs to be
notified and in which order of priority. The contact forms should be hosted online and
offline, as online hosting alone wont be sufficient if the platform is breached

### Incident Response Plan
Organizations should have a formal, focused and coordinated approach to responding to
incidents. The incident response plan should be based on the incident response policy
and provide a roadmap on how to implement incident response capability. The incident
response policy should at a minimum include authorities, roles and responsibilities and be
approved by the senior management. The incident response plan should align with results
from the companies risk assessment, where the organisation assets and vulnerabilities
are identified. However, each organisation have unique requirements, which related to the
size, structure and function of the organization. The plan should include
all the necessary resources and according to the National Institute of Standards and
Technology the following element should be included in the incident response plan:

  - **Mission**
    - The incident response plan should include a mission statement that clearly determine the
purpose of the incident response planning process
    - The mission statement should be
understandable, enabling all involved parties to understand the scope of the work. Terms
and definitions should be outlined, as all involved parties may not be incident responders
or information security professionals
    - The definitions of policy and pro-
cedures should be included in the mission statement and establish a quality framework. 



  - **Strategies and Goals**
    - Strategies and goals should be clearly defined in the incident response plan. The or-
ganisations objectives should be considered when setting incident response strategies
and goals. 
    - An incident response strategy may that the organisation follow the incident
response principles in NIST SP-800-53 control guidelines which includes the incident re-
sponse life-cycle. 


  - **Senior Management Approval**
    - Management approval and support is vital requirement for the incident response plan to
be effective.
    - If no incident response plan exist within the organisation, the senior manage-
ment must approve the establishment of such a plan and likely the approval from executive
management, as the plan should cover the entire organization. 


  - **Organization Approach to Incident Response**
    - It should be clear what the organisations is protecting and why that assets needs protection
    - Inventory assets and which systems are at risk should be clearly stated and protective measures must
be implemented based on how critical the assets are for business operations
    - he incident response
plan should include who has the authorization to declare an incident and when the incid-
ent is declared the incident response team should invoke the incident response plan to
handle the incident



  - **How the Incident Response Team will Communicate with the Rest of the Organiza-
tion and with other**
    - How the communication flows during an incident must be clearly defined in the incident
response plan and aligned with the incident response policy.
    - It should at a minimum
include how the incident response team will communicate securely during an incident,
where the incident response team will meet, what should be communicates written and
verbally.
    - The incident response plan should clearly state who communicates with external
parties and who is responsible to report to the top management


- **Roadmap for Maturing the Incident Response Capability**
Downtime in critical systems are expensive for organisations and affect revenue and pro-
ductivity, therefor, its important for the organisation to track key performance indicators in
the incident response process. Improvement of the incident response process require that
the organisation have a plan about how and when to measures the performance of the
incident response team. Key performance indicators and metrics varies in organisations.
Closely monitoring and tracking key performance indicators may help the organizations to
identify and improve the organisations incident response process and set realistic goals
for the incident response team. Example of Key performance indicators may be: 
  - Number of Incidents 
  - Amount of time spent on the incident 
  - Time spent from discovery of the incident, including time spent in each phase (i.e,
containment, recovery). 
  - How long it took to report the incident
  - Average cost per incident
  - Number of incidents based on type and category
  - Number of incident based on impact and priority


The quality of information technology related services are often evaluated by the reliability
of the systems and uninterrupted business operations. Information security incident and
reliability are closely linked. Service level agreements (SLA) is an agreement between two
entities, which address the following questions; what is delivered, where is it delivered and
17
when is it delivered? Service level agreements should include the most critical elements
that the incident response team monitor. When the organisation conduct a risk assess-
ment, the results and recommendation based on the report should be used in the service
level agreement 


### Incident Response Procedure
Incident response procedures should be based on the incident response plan and policy.
Standard operating procedures should be based on the incident response policy and
plan. Standard operating procedures are a description of specific technical processes,
techniques, forms and checklists used by the incident response teams. The standard
operating procedures should be detailed and comprehensive and reflect the priorities of
the organisations incident response process. Organizations benefit from having clearly
defined and detailed procedures as they minimize errors, especially errors caused by
stressful situations in the incident handling process. Procedures should
be created for different types of information security incidents, as the handling process
differs. The procedures should additionally cover the incident handling process from preparation to lessons learned. 




## 2- Preparation
Compile a list of all assets within the organization is crucial
to identify the assets and analyse the likelihood and impact of the assets if exploited. 

  - Risk assessment involves identifying the threats, risks, likelihood and exposure of the
organizational assets. Due, to lack of skilled personnel and technical competency within
small and medium sized enterprises, these following questions are included as a guideline
questions the organization should answer during the risk assessment: 

    - 1. Valuable data within the organization
    - 2. Who handles and where is sensitive data stored 
    - 3. What type of security controls exists within the organization
    - 4. Can any of the identified information lead to a breach within the organization?




  - The organization should at a minimum consider these aspects:
    - Servers
    - Networks
    - Applications
    - Critical Endpoints
    - Employees
    - Security Products
    - Install & Maintain tools 
    - Training


- Plan Exercises
  - Traning 
  - Table Top
  - Hands On

## 3- Detection and Analysis
The main purpose of the detection and analysis phase is determine if an incident is occuring and analyse the nature of of the incident. The incident response team should not start eradication without a proper analysis, as the incident response team might waste resources on an events that is not an incident. Incident resporting and and actions should be based on the analysis results. 



This section should include the following informations
### Monitor Endpoints
- Endpoint monitoring track activity and risks on all mobile devices connected to the organizations network. Visibility and insight to all endpoints is crucial to be able to track and detect anomalies within the network. 
Key points for organizations to consider during endpoint monitoring is: 

    - Are all the devices on authorized 
    - Are all applications on the endpoint updated and patched 
    - Is the employee accessing shared files and sensitive data 
    - Is the traffic on the endpoint normal 
    - Is there any malware on the endpoints
### Network Traffic
- When an incident is identified, a detailed analysis of the network traffic can attribute the steps of an attacked and potentially determine the indicator of compromise. Network traffic can be captured with several tools: 
  - Tcpdump: Packet capture tool
  - Tshark: Packet capture tool 
  - Wireshark: Packet capture tool 
  - PassiveDNS: DNS logging tool 
  - Snort: Intrusion detection tool 
  - Nfdump: Netflow tool
  - Exiftool: Artifact analysis tool 


- Incident handlers within the organization should be familiar with protocols and potential risks, the following protocols are often exploited by attackers: 
    - RDP : Remote Desktop Protocol is commonly used by attackers to get persitence within the network. Accounts with RDP enabled should be monitored for suspicious activity. 
    - SMB : Service Message Block protocol is widely used by attacker and often targeted by malware. Traffic on thos protocol should me monitored and permissions and file accesses should monitored closly. 
    - SSH : Secure shell connections is a protocol incident responders should monitor. How are remote SSH connections allowed into the network, what type of privileges does the user account have. 
    - SNMP :  Simple Network Management Protocol reavels a lot of information about hosts within the network. Incident responders should look at traffic on this protocol, and determine if the traffic is acceptable. 
    - FTP : File Transfer Protocol is insecure. Organizations should not use this protocol for sensitive files. 



  
    
### Logs 
- Application logs: Organization run carious applications such as web-servers, databases, etc. Applications generalte log-data, which can provide insight on what is happening with the Application. 
  - logs generated from applications such as databses indicate request and quries from users, which can help detect unauthozied file access and data manipulations. 
  - Windows Event logs:
    - Windows Application logs: 
      - Windows application logs are events in the Windows operating system. i.e., an error forcing an application to close is logged in the application log. 
      - Security logs: 
        - Security logs within windows are events that affect the security, which include failed login attempts and file deletion 
      - System logs 
        - System logs contain events that are logged by the operating system, these logs have information on if the process and drivers are loaded successfully.  
      - Directory Service logs: 
        - These are of importance if the organization is using Active Directory Service. Directory Service logs record modification and authentication of privileges  
      - Dns Service logs: 
        - Generate logs from the DNS server, and includes information such as queried domains, client IP addresses, and requested records 
      - File Replication Service logs: 
        - Provides information on events of the domain controller replication. 



    
    - User activity logs / Endpoint logs
     
      Passwords may be breached from phishing attacks, which further can be used to log into the systems. User activity logs help to detect suspicious traffic. 
      - History
        - Determine of a new user started logging into the system? 
        - Determine if the user started moving laterally within the system? 
      - User 
        - Are users logging into systems that are not realted to their role? 
        - Are the user laterally moving close to assets within the organization? 
      - Time 
        - Are users logging in a suspicious times? 
      - Remote host 
        - Is there a user that logs in remotely from unexpected systems? 
      - Brute force 
        - Is there any signs of excessive failed logins? 
     
     
      








  - Operating system, service and application logs 


  - Network device logs




### Indicators of Compromise 
Indicators of compromise is foresnic evidence within the network or operating systems, which with confidence indicates a computer intrusion. 
- TTP 
  - Tools, techniques and procedures characterize how adversaries behave, including how they are doing it. TTPS are observed from spesific incidents and contextuale the understanding accross incidents and threat actors. Studing TTPs help the organization understand how adversaries execute their attacks. 
    - Using this knowledge help organization focus their actions appropriatly. 
    - Studing TTPS help organization identify possible attack vectors, which again can be used to determine which systems that are likely to be attacked. 
- Hashes 
  - When the organization system has fallen victim to attacks, malicious files might be installed on the system. These files are often masquerade themself as legitimate files. These malicious files can record keyboard strokes, obtain valueable information and track user activities, to mention a few. If the organization suspect malicious files on their system, the hash value of the file should be compared with the hash value provided by a reliable threat intelligence source. 

  - Threat intelligence sources can provide information like:
    -  Processes executed by malicious files 
	  - IP addresses and C2 server associated with the files. 
  	- Type of attack normally carried out by these files.
- Ip Addresses
    - IP addresses should be checked towards threat intelligence sources to determine if they are affliated with malicious activities 
  	- Autonomous System Number (ASN) is a collection of routing prefixes under the control of network operators. These are ranked based on the number of malicious IP addresses they contain and their association with malicious command and control infrastructure.
	  - High ASN score may indicate that the external IP-address is malicious.   
- URL/domains 
   - Domains anre URLs are typical indicators of comporimse as they are used in various attacks, including phishing and malware. One of the most common methods are URL redirection attacks, which redirects to malicious page, rather than the page the URL indicates.
    	- Third party lookup tools can be used to search for malicious URLS, such as VirusTotal.  



### Common Attack Vectors 
- Compromised Credentials / Weak Credentials 
    - Compromised username and passwords is still one of the most common attack vectors. However, this type of attack often happens when employees fall prey to phishing attacks and expose credentials on fake websites. The risk of compromised credentials varies based on the level of access the employee has. Compromised credentials is not limited to employees, but network devices, servers and security tools often use a password based approach to enable communication flow between devices. Compromised credentials on these often allow for vertically and horizontally movement within the network, ultimately giving the attackers high privileged access. 
    - Weak credentials is one of the most common attack vectors, due to individuals continue to use weak passwords and resuse passwords across various platforms 

- Malicious Insider 
  - Insider threats are attacks that come from inside the organization, often through employees who expose confidentials information to attackers and third parties. Organization should monitor network access and employees who access systems or files they should not access, which can be an indicator of a malicous insider. 

- Missing or Poor Encryption 
  - Weak or missing encryption lead to organizational data being transmitted in cleartext. This pose a risk to information being exposed unauthorized parties,  through brute force attacks. 
   

- Phishing 
  - Phishing attacks are the most common incident faced by small and medium sized enter-
prises, according to ENISA. Phishing attacks is a common attack vector
in organisations of all sizes with online facing communication. Phishing attacks is a form
of social engineering attack where the attacker masquerade a person or a entity in email
or other form of communication. Attackers often use phishing emails as their initial access
point and are used in roughly 80% of reported security incident (Manoj Srivastava, 2021).
Phishing emails hit organisations of all sizes and can reach millions of users. Attackers
use public sources to gain background information of employees and organisations by
checking personal history, interest and activities to craft the attack. Employees can be hit
24
be a mass campaign where the attacker send the same email to a mass of users or a
targeted campaign where the attacker has gathered information about the victim before-
hand.


 ## References: 
 https://www.balbix.com/insights/attack-vectors-and-breach-methods   
https://gitlab.com/syntax-ir/playbooks 
https://resources.infosecinstitute.com/topic/network-traffic-analysis-for-incident-response-ir-what-incident-responders-should-know-about-networking/   
https://www.exabeam.com/explainers/siem/event-log/    
https://www.upguard.com/blog/attack-vector   
https://www.enisa.europa.eu/news/enisa-news/phishing-most-common-cyber-incidents-faced-by-smes   
https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-61r2.pdf  
https://www.trendmicro.com/vinfo/us/security/definition/indicators-of-compromise  
https://www.cyberseer.net/wp-content/uploads/2018/11/Cyberseer-UK-Sec-Show-From-IOC-to-TTP-How-Attack-Chains-Have-Evolved.pdf   
https://cyberhoot.com/cybrary/tactics-techniques-and-procedures-ttp/
https://www.manageengine.com/products/eventlog/cyber-security/md5-hash-iocs.html 
https://www.manageengine.com/products/eventlog/cyber-security/external-ip-iocs.html 





## IRP-*
IRP folders will contain the actual incident spesific playbooks, example IRP-PHISHING. 



