# Ransomware Playbook

[[_TOC_]]

## Scope
This Playbook covers preparation and detection of ransomware. 



## 1. Preparation


### Devlop Incident response plan 
- An overveiw of what should be included in the incident response plan are provided on the first page. 
     - Risk assessment 
     - Policies and procudures 
     - Training 
     - Communcation 

### Asset List
Compile a list of all assets within the organization is crucial
to identify the assets and analyse the likelihood and impact of the assets if exploited. 

  - Risk assessment involves identifying the threats, risks, likelihood and exposure of the
organizational assets. Due, to lack of skilled personnel and technical competency within
small and medium sized enterprises, these following questions are included as a guideline
questions the organization should answer during the risk assessment: 

    - 1. Valuable data within the organization
    - 2. Who handles and where is sensitive data stored 
    - 3. What type of security controls exists within the organization
    - 4. Can any of the identified information lead to a breach within the organization?




  - The organization should at a minimum consider these aspects:
    - Servers
    - Networks
    - Applications
    - Critical Endpoints
    - Employees
    - Security Products
    - Install & Maintain tools 
    - Training


### Backups
- Backups are not a protective stategy against ransomware attacks, but they are critical compoenent for the recovery process. Without backups, or backups that cannot be restored, the organization is left with limited options for recovery. Creating ransomware-resistant backups is a key components in prepearing for ransomware attacks. 

- The backup strategy within the organization should at a minimum follow the 3-2-1 rule. 
     - 3 copies of data
     - 2 different media types (on-premsis and cloud-based)
     - 1 backup stored at an offsite location. 

- Multiple backup systems that segment the network. 
    - Full backup 
        - A full backup is a complete copy of all organizational data assets. 
    - Differential 
        - Differential backup is databack that copies the files that have changed since the last full backup was conducted. Which includes any data that has been updated, created or alterneted. This type of backup do not copy all of the data every time. 
    - Incremental 
        - Incremental backup require one full backup to be made, and then backup the data that has been changed since the last full backup. 







   


### Email template Example: 

    Dear [XXX], 

    This email is to inform you of a secutity incident, which was recently reported relating to RANSOMWARE attack in [APPLICATION] on [DATE]. 

    We [ORGANIZATION] are aware of the incident, and have launched an investigation. Based on the evidence, we [ACTIONS] to resolve the issue. 

    **What can you do**

    The most common way ransomware penetrate the systems are through emails. These often include malicious links or attachments. 
    - Do not open attechment from unknown senders, be aware of compressed files and executables. 
    - Do not send sensitive information like usernames and passwords over email. 
    - Always inspect URLs before clicking on them. 
    - Do not open shared documents that youre not expecting. 

    If you have any questions regarding the incident, please send an email to [INSERT EMAIL ADDRESS]. 

    Best Regards,   
    [NAME]  
    [ROLE]  
    [COMPANY]  


### Common Ransomware Attack Vectors 
- Phishing 
- Exoposed Services 
    - Remote Protocol Desktop 
    - Content management systems 
- Drive-by Download 
    - Visit infected website   
- Password Spraying   


### Network Segmentation 
- Dividing the network into smaller section. 
    - Allows for isolation and possibly the spread of malware to different sections of the organizations network. 
    - Control restricted areas 

### Disable macros 
- Common attack vector for ransomware is through phishing attacks. 



 

</details>

## 2. Detect



-  Objective   
     - Detect incident 
     - Determine scope 
        - Involve third party to analyse, contain, remidate and recover 



 

### Workflow



![Ransomware Workflow](Workflow/Ransomware.png.png)







### Alerts
- EDR / AV 
    - Endpoint detection and response monitor the end users device accross the network. 
        - Stolen valid credentials normally wont trigger an alarm, however, priviledge elevation and horizonally moving to other systems are likely to get flagged my endpoint detection and response. 
    
- Reports 
    - Dns 
    - Web Proxy 
- Windows Event Alert 
- File Integrity Alert 



### Notifications
- Employee Discovers Ransomware
- Third Party Notification
- Internet Service Provider 
- Computer is slow 
- Computer crashed unexpectedly 







### Indicators of Compromise 
Indicators of compromise such as attachments and URLs discussed in the section above should be validated. 
- URLs can be checked on various sites such as: 
    - VirusTotal 
    - URLscan 
    - Hybrid Analysis 

- Hashes from files should be validated. 
    - Powershell can be used to get the file hash checksum of files. 
        - get-filehash -path "C:\Users\username\Desktop\fileyouwantocheck" | format-list
        - Output 
            - Algorithm : SHA256
            - Hash      : AB37597D5D19C4C719763BE980F15418EBC9CE862DDBD4EE40040543A2D9691A - This is what you want to check 
            - Path      : C:\Users\username\Desktop\fileyouwantocheck 

    - The hfile hash checksum can be checked towards sites such as: 
        - VirusTotal 
        - Hybrid Analysis 

    - **NB: Adversaries may use sandbox evasion techniques to prevent the malware from running inside a sandbox, meaning that VirusTotal, antivirus solutions, etc. will not detect the file as malicious from its dynamic analysis. For example, the malware might require user interaction to be executed.**


### Contact Incident Response Team 
As discussed above, realiable backups that are securly stored offline enhance the organizations ability to recover from ransomware attacks. However, if the organizations is infected with ransomware some immidiate actions can be taken to minimize the impact of the infection. However, these should be disuccsed with the incident response team so the first responder do not end up destorying evidence. 
- Communicate with the incident response team (This should established in the incident response plan)
- Document all details to ensure that incident response team have an initial understand of the incident
- Triage affacted system to assist the incident respond to focus on immediate actions. 
    - Determine which devices and/or systems that are infected with ransomware. 
    - Isolate system and devices that are infected. 
    - Disconnect infected system 
        - Disconnect from the internet 
    - Inform all stakeholders of compromised data 
    - Disable:  
        - Virtual Private Networks 
        - Remote Access Servers 
        - Cloud based or public-facing assets 



### Reporting an Incident, weakness or improvement 
This is an example of how organizations can report and register security inicdents or weakness and ideas for security improvement: 
1. Use corporate email to rport an Information Security Incident 
2. Address the email to "Securityincident@Organization.com" - remember to CC the organizations incident manger. 
3. Subject line: Country, Location, Office, "Event subject" - Incident, weakness, improvement. 
Example: 
    - Norway, Kristiansand: Broken Door - Security weakness 
    - Norway, Bergen: Insider - Data Breach - Security Incident 
4. In the main body of the email, describe the matter of the incident. Including the following: 
    - Time and Date of the security incident 
    - WHO witnessed the security incident 

The email should include as much detail as possible, including what data or systems is involved, the nature of the incident and who to contact for more information. 

















  



 







</details>
</details>
</details>



</details>

# References
https://www.becybersafe.com/online/email-phishing-advanced.html     
https://www.arcsystems.co.uk/the-dangers-of-links-and-attachments-in-emails/   
https://blog.focal-point.com/cyber-security-awareness-free-email-templates   
https://ransomware.org/how-to-prevent-ransomware/passive-defense/ransomware-backup-strategy/  
https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-61r2.pdf   
https://www.gov.scot/publications/cyber-resilience-incident-management/
https://cyber.gc.ca/en/guidance/ransomware-playbook-itsm00099  
https://gitlab.com/syntax-ir/playbooks    
https://www.gov.scot/publications/cyber-resilience-incident-management/  






